/*! \dist_pts.cpp
 * Brief description: Calculate the distance between 2 points.
 *
*/
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	// Auxiliary variables
	float x1, x2, y1, y2, d;

	// Info input
	cout << "Type the first point [x1, y1]" << endl;
	cin >> x1 >> y1;

	cout << "Type the second point [x2, y2]" << endl;
	cin >> x2 >> y2;

	// Calculation
	d = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));

	// Print the result
	cout << "Result: " << d << endl;

	return 0;
}
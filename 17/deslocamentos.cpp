/*! \deslocamentos.cpp
 * Brief description: Main program.
 *
 * Detailed description: Change element position.
*/

#include <iostream>

using namespace std;

// Function to change values
void Swap( int &n1, int &n2 )
{
    int aux=0;
    aux = n1;
    n1 = n2;
    n2= aux;
}

// Function to print array
void print_array( int A[ ], int sz_ )
{
    cout << "vet: [ ";
    for( auto i(0); i < sz_ ; ++i )
    {
        // Print array
        cout << A[i] << " ";
    }
    cout << "] " << endl << endl;
}

// Change position accordling to the value
void ShiftN( int myVector[], int &desloc )
{    
    if ( desloc > 0 )
    {
        //Para direita
        for( int j = 0; j < desloc; j++ )
        {
            Swap(myVector[2], myVector[3]);
            Swap(myVector[1], myVector[2]);
            Swap(myVector[0], myVector[1]);
        }
    }
    else
    {
        if ( desloc < 0 )
        {
            desloc *= -1;
            // To the left
            for( int j = 0; j < desloc; j++ )
            {
                Swap(myVector[0], myVector[1]);
                Swap(myVector[1], myVector[2]);
                Swap(myVector[2], myVector[3]);
            }
        }
    }

    //Imprime na tela.
    print_array(myVector, 4);
}

int main()
{
    // Auxiliary variables
    int sz = 4;
    int meuVetor[sz] = {0};
    int deslc;

    // Values insertion
    for ( int i = 0; i < sz; i++ )
    {
        cout << "Digite o valor de n" << i+1 << ":" <<endl;
        cin >> meuVetor[i];
    }

    cout << "Insira o deslocamento: " << endl;
    cin >> deslc;

    ShiftN(meuVetor,deslc);

    return 0;
}

/*
 * Escreva um programa em C/C++ chamado troca interna.cpp que lê 20 inteiros, os
 * armazena em um arranjo unidimensional (vetor) A e o imprime na tela. A seguir, o
 * programa deve trocar o conteúdo do último elemento de A com o 1o, do penúltimo
 * com o 2o, do ante-penúltimo com o 3o e assim pode diante até que todos os elementos
 * tenham sido trocados de lugar apenas uma vez. Por fim, o programa deve imprimir o
 * vetor modificado.
 */

#include <iostream>
#include <cstdlib>
using namespace std;


//------------------------------------------------------------------------------------
//! Lê um vetor de inteiros.
/*! Esta função lê um vetor de inteiros cujo o tamanho é passado como argumento.
 * @param A O vetor a ser lido.
 * @param sz O tamanho do vetor passado como argumento.
 */  
void readArray(int A[ ], int sz)
{
	//le o vetor
	for(int i(0); i<sz ; ++i)
	{
		cin >> A[ i ];
	}
}

//------------------------------------------------------------------------------------
//! Exibe na tela um vetor de inteiros.
/*! Esta função Exibe um vetor de inteiros cujo o tamanho é passado como argumento.
 * @param A O vetor a ser exibido.
 * @param sz O tamanho do vetor passado como argumento.
 */
void printArray(int A[ ], int sz)
{
	//exibe o vetor
	for( int i(0) ; i < sz ; ++i )
	{
		cout << A[ i ]<<" ";
	}
}

//------------------------------------------------------------------------------------
//! Troca os elementos do vetor de posição.
/*! Esta função troca os elementos do vetor de posição, o primeiro pelo ultimo, 
 *  o segundo pelo penultimo, o terceiro pelo antepenultimo e assim em diante.
 * @param A O vetor a ser exibido.
 * @param sz O tamanho do vetor passado como argumento.
 */
void changeElements(int A[ ], int sz)
{
	int count = sz-1;
	int aux1, aux2;

	for( int i(0) ; i < 3 ; ++i )
	{
		aux1 = A[count];
		aux2 = A[i];
		A[i] = aux1;
		A[count] = aux2;
		count--;
	}
}

int main()
{
	int Vetor[5];
	cout <<"Digite 5 numeros inteiros"<<endl;
	readArray(Vetor,5);
	cout <<"Exibicao dos numeros digitados."<<endl;
	printArray(Vetor,5);
	changeElements(Vetor,5);
	printArray(Vetor,5);

	cout <<"Davysson é um cara legal!"<<endl;


	cout <<"Fim";
	
	return EXIT_SUCCESS;
}
#include <iostream>
#include <locale.h>
#include <math.h>

using namespace std;

typedef struct _Ponto {
    int x;
    int y;
    int z;
} Ponto;

//Ver se ta dentro do circulo
void dentroCirc(const Ponto* c, const Ponto* p, const int r){
    //Calcular distancia do ponto central
    float tamanho = (float)sqrt((p->x-c->x)*(p->x-c->x)+(p->y-c->y)*(p->y-c->y)+(p->z-c->z)*(p->z-c->z));

    if(tamanho==r){
        cout << "Esta na superf�cie do c�rculo"<< endl;
    }else{
        if(tamanho<r){
            cout << "Esta dentro do c�rculo"<< endl;
        }else{
             cout << "N�o est� dentro do c�rculo"<< endl;
        }
    }
}

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL, "Portuguese");
    //Vari�veis
    Ponto c,p;
    int r;

    //Atribui��es de valores
    cout << "Digite o valor do raio: ";
    cin >> r;

    cout << "Digite o valor de x do centro do circulo: ";
    cin >> c.x;

    cout << "Digite o valor de y do centro do circulo: ";
    cin >> c.y;

    cout << "Digite o valor de z do centro do circulo: ";
    cin >> c.z;

    cout << "Digite o valor de x do ponto p: ";
    cin >> p.x;

    cout << "Digite o valor de y do ponto p: ";
    cin >> p.y;

    cout << "Digite o valor de z do ponto p: ";
    cin >> p.z;

    //Fun��o
    dentroCirc(&c,&p,r);

return 0;
}

/*! \negativo5.cpp
 * Brief description: Drive to test classification.
 *
 * Detailed description: Receive indeterminated number of values
 * and classificate in different classes.
*/
#include <iostream>

using namespace std;

int main()
{
  // Auxiliary variables
  auto x(0), tot(0);
  auto class01(0); // Numbers from 0 to 25
  auto class02(0); // Numbers from 25 to 50
  auto class03(0); // Numbers from 50 to 75
  auto class04(0); // Numbers from 75 to 100
  auto class05(0); // Numbers greater than 100

  // Input
  cout << "Entre com valores inteiros (Ctrl+d p/ encerrar): " << endl;

  while ( cin >> x )
  {
      tot += 1;
      // Check the class
      if ( x >= 0 && x < 25 )
        class01 += 1;
      else if ( x >= 25 && x < 50 )
        class02 += 1;
      else if ( x >= 50 && x < 75 )
        class03 += 1;
      else if ( x >= 75 && x <= 100 )
        class04 += 1;
      else if ( x > 100 )
        class05 += 1;
  }

  cout << "Numeros de 0 a 25:\t" << class01 << " ocorrencias\t" << ((float)class01/tot) * 100 << '%' << endl;
  cout << "Numeros de 25 a 50:\t" << class02 << " ocorrencias\t" << ((float)class02/tot) * 100 << '%' << endl;
  cout << "Numeros de 50 a 75:\t" << class03 << " ocorrencias\t" << ((float)class03/tot) * 100 << '%' << endl;
  cout << "Numeros de 75 a 100:\t" << class04 << " ocorrencias\t" << ((float)class04/tot) * 100 << '%' << endl;
  cout << "Numeros maiores que 100:\t" << class05 << " ocorrencias\t" << ((float)class05/tot) * 100 << '%' << endl;

  // Program return
  return EXIT_SUCCESS;
}
#include <iostream>
#include <locale.h>

using namespace std;

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL,"Potuguese");
    //Variáveis
    int **m;
    int n;
    int cont=1;

    //Definindo tamanho
    cout << "Digite o tamanho da matriz: ";
    cin >>n;

    //Aloca os ponteiros
    m = new int*[n];

    //Aloca o tamanho de cada ponteiro
    for(int i=0;i<n;i++){
        m[i] = new int[n];
    }
    //Insere os valores
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            m[i][j]=cont++;
        }
    }

    //Imprime na tela
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout << m[i][j] << " ";
        }
        cout << "\n";
    }
return 0;
}

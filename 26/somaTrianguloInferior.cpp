#include <iostream>
#include <locale.h>

using namespace std;

int somaTrianguloInferior(float **m, int n){
    float soma=0;
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(i>j){
                soma+=m[i][j];
            }
        }
    }
    return soma;
}

int main(){
    //Permite palavras acentuadas
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    float **m;
    int n;

    //Definindo tamanho
    cout << "Digite o tamanho da matriz: ";
    cin >>n;

    //Aloca os ponteiros
    m = new float*[n];

    //Aloca o tamanho de cada ponteiro
    for(int i=0;i<n;i++){
        m[i] = new float[n];
    }

    //Insere os valores
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout << "Digite o valor da linha " << i+1 << " coluna " << j+1 << ": ";
            cin >> m[i][j];
        }
    }

    cout << "A soma �: " << somaTrianguloInferior(m,n) << endl;
 return 0;
}

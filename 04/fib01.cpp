/*! \fib01.cpp
 * Brief description: Print the Fibonacci sequence.
 *
 * Detailed description: Receive a number L and print the Fibonacci series
 * until this number.
*/
#include <iostream>

using namespace std;

int fibo( int n )
{
    if(n==1)
      return 1;
    if(n==2)
        return 1;
    else
      return fibo(n-1) + fibo(n-2);
}

int main()
{
    // Auxiliary variable
    auto l(0), i(1), fib(0);
    cout << endl << "Inform the limit for Fibonacci series: ";
    cin >> l;

    // Print result
    cout << "[ ";
    while ( true )
    {
        fib = fibo(i);
        if ( fib < l )
          cout << fib << " ";
        else
          break;
        i++;
    }
    cout << "] " << endl << endl;

    return 0;
}
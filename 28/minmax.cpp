#include <iostream>
#include <locale.h>

using namespace std;

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL,"Potuguese");
    //Variáveis
    int m[10][10]={0};
    int maior, coluna;

    //Adicoionar valores
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            m[i][j]=i+j;
        }
    }

    //Imprimindo na tela
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            cout << m[i][j] << "  ";
        }
        cout << "\n";
    }

    //Pesquisando o maior
    maior = m[0][0];
    coluna = 0;

    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            if(m[i][j]>maior){
                maior = m[i][j];
                coluna=j;
            }
        }
        cout << "\n";
    }

    //Encontra o menor termo
    for(int i=0;i<10;i++){
        if(m[i][coluna]<maior){
            maior = m[i][coluna];
        }
    }

    cout << "Minimax: " <<  maior << endl;

return 0;
}

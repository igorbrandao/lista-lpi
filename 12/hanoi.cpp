/*! \hanoi.cpp
 * Brief description: Main program.
 *
 * Detailed description: Implements hanoi game recursive.
*/

#include <iostream>
#include <cstdlib>

using namespace std;

void hanoi( int diskSize, int source, int dest, int spare, int * moves )
{
    *moves += 1;

    if( diskSize == 0 )
    {
        cout << "Move disk " << diskSize << " from tower-" << source << " to tower-" << dest << endl;
    }
    else
    {
        hanoi(diskSize - 1, source, spare, dest, moves);
        cout << "Move disk "  << diskSize << " from tower-" << source << " to tower-" << dest << endl;
        hanoi(diskSize - 1, spare, dest, source, moves);
    }
}

int main( int argc, char const *argv[] )
{
    // Auxiliary variables
    auto moves(0);

    // Check the input
    if ( argc > 1 ) 
    {
        // Call function to process hanoi game
        hanoi(atoi(argv[1]) - 1, 0, 1, 2, &moves);
        cout << "It was necessary " << moves << " move(s)" << endl;
    }
    else
    {
        cout << endl << "Inform at least the number of disks" << endl << endl;
    }

    return 0;
}
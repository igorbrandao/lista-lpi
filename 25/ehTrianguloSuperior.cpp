#include <iostream>
#include <locale.h>

using namespace std;

//Fun��o para ver se � matriz triangular superior
int ehTrianguloISuperior(int* m,int n){
     //Descrec�mo para igualar acrescimo posterior
     m--;
     //Percorre toda a matriz
     for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            //Acrescimo para ir termo a termo na matriz
            m++;
            if(j<i){
                if(*m!=0){
                    return 0;
                }
            }
        }
    }
    return 1;
}

int main(){
    //Permite palavras acentuadas
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    int n;
    int cont=1;

    //Definindo tamanho
    cout << "Digite o tamanho da matriz: ";
    cin >>n;

    //Cria��o da matriz
    int m[n][n];

    //Insere os valores
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout << "Digite o valor da linha " << i+1 << " coluna " << j+1 << ": ";
            cin >> m[i][j];
        }
    }
    //Executa a fun��o
    if(ehTrianguloISuperior(m[0],n)){
        cout << "� uma matriz triangular superior";
    }else{
        cout << "N�o � uma matriz triangular superior";
    }

return 0;
}

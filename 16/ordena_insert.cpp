/*! \ordena_insert.cpp
 * Brief description: Main program.
 *
 * Detailed description: Compare an int value with an entire array.
*/

#include <iostream>

using namespace std;

void print_array( int * A[ ], int sz_ )
{
    cout << "vet: [ ";
    for( auto i(0); i < sz_ ; ++i )
    {
        // Print array
        cout << A[i] << " ";
    }
    cout << "] " << endl << endl;
}

void organize_array( int A[ ], int sz_ )
{
	// Auxiliary variables
    auto aux(0);
	for( auto i(0); i < sz_ ; ++i )
    {
        for( auto j(1); j < sz_ ; ++j )
	    {
	        if ( A[i] > A[j] )
	        {
	        	aux = A[i];
	        	A[i] = A[j];
	        	A[j] = aux;
	        }
	    }
    }
}

int main()
{
    // Auxiliary variables
    int vet[] = {1, -2, 3, 0, -5, 6, -7, 8, 9, -10,
             11, 12, -13, 14, 0, 16, 0, 18, -19, 20};
    int sz = sizeof(vet)/sizeof(int); // Determine the array length

    #ifdef MANUAL_INPUT
      // Array input
      auto fxSz(20);
      for( auto i(0); i < fxSz ; ++i )
      {
        cout << "Digite o elemento " << (i+1) << " do vetor: " << endl;
        cin >> vet[i];
      }
    #endif

    // Organize the vector
    organize_array(&vet, sz);

    // Print original vector
    print_array(vet, sz);

    // Fill the vector
    /*for ( auto idxFast(0); idxFast < 20; ++idxFast )
    //for ( auto idxFast(0); idxFast < sz; ++idxFast ) // ????
    {
        if ( vet[idxFast] > 0 )
            vet[idxSlow++] = vet[idxFast];
    }

    // Print new vector
    print_array(vet, idxSlow);*/

    // Program return
    return EXIT_SUCCESS;
}
/*! \troca_interna.cpp
 * Brief description: Change the latest positions with the first ones.
 *
*/
#include <iostream>

using namespace std;

void print_array( int A[ ], int sz )
{
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Print array
		cout << A[i] << " ";
	}
	cout << "] " << endl << endl;
}

void change_elements( int A[ ], int sz )
{
	int count = sz-1; // last position
	int aux1, aux2;

	for( int i(0) ; i < 3 ; ++i )
	{
		aux1 = A[count];
		aux2 = A[i];
		A[i] = aux1;
		A[count] = aux2;
		count--;
	}
}

int main()
{
	// Auxiliary variables
	int vet[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
				   11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	int sz = sizeof(vet)/sizeof(int); // Determine the array length

	#ifdef MANUAL_INPUT
		// Array input
		auto fxSz(20);
		for( auto i(0); i < fxSz ; ++i )
		{
			cout << "Digite o elemento " << (i+1) << " do vetor: " << endl;
			cin >> vet[i];
		}
	#endif

	// Print original array
	print_array(vet, sz);

	// Change elements
	change_elements(vet, sz);

	// Print new array
	print_array(vet, sz);

	return 0;
}
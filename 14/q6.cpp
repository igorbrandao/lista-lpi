/*! \q6.cpp
 * Brief description: Implement the functions form .h.
 *
 * Detailed description: Basic operations
*/
#include <iostream>
#include "q6.h"

using namespace std;

/* Function to compare a single value with an array */
int maiores ( const int* const vet, const int n, const int x )
{
	auto result(0);

	// Verify problematic inputs
	if ( n <= 0 ) return -1;

	// Run trough vector
	for ( auto i(0); i < n; ++i )
	{
		if ( vet[i] > x )
			result++;
	}
	return result;
}
/*! \invert_array.cpp
 * Brief description: Main program.
 *
 * Detailed description: Change element position.
*/

#include <iostream>

using namespace std;

// Invert array
void invert_array( int* vetor, int &tam )
{
    int *p = vetor;
    int aux = 0;
    p += tam-1;

    // Invert array, run until the mid
    for(int i = 0 ; i < (tam/2); i++, vetor++, p--)
    {
        aux = *(vetor);
        *(vetor)= *(p);
        *(p)= aux;      
    }
}

void print_array( int A[ ], int sz_ )
{
    cout << "vet: [ ";
    for( auto i(0); i < sz_ ; ++i )
    {
        // Print array
        cout << A[i] << " ";
    }
    cout << "] " << endl << endl;
}

int main()
{
    // Auxiliary variables
    int tam;
    int *v = NULL;

    // User input
    cout << "Digite o tamanho do vetor: " << endl;
    cin >> tam;

    v = new int[tam];

    // Insert values in vector
    for ( int i = 0; i < tam; i++ )
    {
        cout << "Digite o valor da " << i+1 << " posicao do vetor" << endl;
        cin >> v[i];
    }

    // Call the function
    invert_array(v, tam);
    print_array(v, tam);

    return 0;
}
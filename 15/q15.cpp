#include <iostream>
#include <cstdlib>

using namespace std;

/* Function to change variables value by reference */
void Swap( int * value01, int * value02 )
{
    // Auxiliary variables
    auto aux(0);

    // Swap
    aux = *value01;
    *value01 = *value02;
    *value02 = aux;
}

/* Organize the values position accordling to a flag */
void Ordena3( int * vl01, int * vl02, int * vl03, char flag )
{
    // Check the ordem
    if ( flag == 'V' ) // ascending
    {
        if ( *vl01 > *vl02 )
            Swap(vl01, vl02);
        else if ( *vl02 > *vl03 )
            Swap(vl02, vl03);
        else if ( *vl01 > *vl03 )
            Swap(vl01, vl03);
    }
    else // descending
    {
        if ( *vl01 < *vl02 )
            Swap(vl01, vl02);
        else if ( *vl02 < *vl03 )
            Swap(vl02, vl03);
        else if ( *vl01 < *vl03 )
            Swap(vl01, vl03);
    }
}

/* Function to check if a number is prime */
int EhPrimo( int n )
{
    for ( auto i(2); i <= n/2; ++i )
    {
        if ( n%i == 0 )
        {
            return 0;
        }
    }
    return 1;
}

/* Function to check if a number is even or odd */
char EhPar( int n )
{
    if ( n%2 == 0 ) // even
        return 'V';
    else // odd
        return 'F';
}

int Soma( int N, int S )
{  
    for( auto i(2); i < N; i++ )
    {  
        if ( N % i==0 )
        {  
            S=S+i;
        }  
    }  
    return S;  
}  

/* Function to check if a number is even or odd */
char EhAmigo( int n1, int n2 )
{
    // Auxiliary variables
    auto soma01(0);
    auto soma02(0);

    soma01 = Soma(n1,soma01);
    soma02 = Soma(n2,soma02);

    if( (soma01 == n2) && (soma02 == n1) ) // Amigo
        return 'V';
    else // Não amigo
        return 'F';
   return 0;  
}

/* Return the smaller value */
int Menor( int n1, int n2, int n3 )
{
    if ( n1 < n2 && n1 < n3 )
        return n1;
    else if ( n2 < n1 && n2 < n3 )
        return n2;
    else if ( n3 < n1 && n3 < n2 )
        return n3;

    return 0;
}

/* Function to return the mdc of 3 numbers */
int mdc( int n1, int n2, int n3 )
{
    // Auxiliary variables
    auto menor(0);
    menor = Menor(n1, n2, n3);

    for ( auto i((menor/2)); i >= 2; i-- )
    {
        if ( (n1%i) == 0 && (n2%i) == 0 && (n3%i) == 0 )
            return i;
    }
    return 1;
}

/* Function to return the mmc of 3 numbers */
int mmc( int n1, int n2, int n3 )
{
    // Auxiliary variables
    auto menor(0);
    menor = Menor(n1, n2, n3);

    for ( auto i(0); i <= (menor/2); i++ )
    {
        if ( (n1%i) == 0 && (n2%i) == 0 && (n3%i) == 0 )
            return i;
    }
    return 1;
}

/* Function to return fatorial of a number */
int Fatorial( int n )
{
    if ( n > 1 )
      return n * Fatorial(n - 1);
   else
      return 1;
}

int main()
{
    // Auxiliary variables
    auto var01(5);
    auto var02(10);
    auto var03(15);
    auto flag('V');

    // Testing Swap function
    cout << "Original values: [" << var01 << ", " << var02 << "]" << endl;
    Swap(&var01, &var02);
    cout << "New values: [" << var01 << ", " << var02 << "]" << endl;

    // Testing Ordena3 function
    Ordena3(&var01, &var02, &var03, flag);
    cout << "Ascending values: [" << var01 << ", " << var02 << ", " << var03 << "]" << endl;

    // Testing EhPrimo function
    cout << "Eh primo: " << EhPrimo(13) << endl;

    // Testing EhPar function
    cout << "Eh par: " << EhPar(10) << endl;

    // Testing EhAmigo function
    cout << "Eh amigo: " << EhAmigo(284, 220) << endl;

    // Testing mdc function
    cout << "Mdc: " << mdc(284, 220, 150) << endl;

    // Testing mmc function
    cout << "Mmc: " << mmc(6, 12, 24) << endl;

    // Testing Fatorial function
    cout << "Fatorial: " << Fatorial(8) << endl;    

    return 0;
}
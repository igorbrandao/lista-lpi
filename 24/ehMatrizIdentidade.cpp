#include <iostream>
#include <locale.h>

using namespace std;
//Fun��o para ver se � identidade ou n�o
int ehMatrizIdentidade (int* mat[],int n){
    //Verifica se todos os valores fora as diagonal principal � 0
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(i!=j){
                //Se achar um termo diferente de 0 na matriz fora diagonal principal
                if(*(mat[j]++)!=0){
                    return 0;
                }
            }else{
                //Se achar um termo diferente de 1 na diagonal principal
                if(*(mat[i]++)!=1){
                    return 0;
                }
            }
        }
    }
return 1;
}
int main(){
    //Permite palavras acentuadas
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    int **m;
    int n;
    int cont=1;

    //Definindo tamanho
    cout << "Digite o tamanho da matriz: ";
    cin >>n;

    //Aloca os ponteiros
    m = new int*[n];

    //Aloca o tamanho de cada ponteiro
    for(int i=0;i<n;i++){
        m[i] = new int[n];
    }

    //Insere os valores
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout << "Digite o valor da linha " << i+1 << " coluna " << j+1 << ": ";
            cin >> m[i][j];
        }
    }
    //Executa a fun��o
    if(ehMatrizIdentidade(m,n)){
        cout << "� uma matriz identidade";
    }else{
        cout << "N�o � uam matriz identidade";
    }

return 0;
}

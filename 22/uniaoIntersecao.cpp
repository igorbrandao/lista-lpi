#include <iostream>
#include <locale.h>

using namespace std;

//Intercala o vetores de caracteres
char* intercalaVetores(char* vetA, int tamA, char*  vetB,int tamB ){
    //Vari�veis
    char* v = new char[tamA+tamB];
    int cont=0, menor;

    //Ver quem � menor
    if(tamB<tamA){
        menor = tamB;
    }else{
        menor = tamA;
    }

    //Percorre e atribui os valores do vetor at� quem A ou B n�o tenham mais valores a atribuir ao vetor v.
    for(int i=0;i<menor*2;i++){
        if(i%2==0){
            *(v++) = *(vetA++);
        }else{
            *(v++) = *(vetB++);
        }
    }
    //Atribui o restante
    if(tamA!=tamB){
        //Caso o vetA seja o menor
        if(menor==tamA){
            for(int i=0;i<tamB-tamA;i++){
                *(v++) = *(vetB++);
            }
        }else{
            //Caso vetB seja o menor
            for(int i=0;i<tamA-tamB;i++){
                *(v++) = *(vetA++);
            }
        }
    }

    return v = v-tamA-tamB;
}


void uniaoIntersecao( int *vetA, int tamA, int *vetB, int tamB,int *vetUni, int& tamUni,int *vetInt, int& tamInt ){
    //Vari�veis
    vetA = new int[tamA];
    vetB = new int[tamB];
    vetUni = new int[tamA+tamB];
    vetInt = new int[tamA+tamB];

    //Tamanho dos valores
    tamInt = tamA+tamB;
    tamUni = tamA+tamB;

    //Atrinui��o de valores do vetor A
    for(int i=0;i<tamA;i++){
        cout << "Digite o valor da " << i+1 << "� posi��o do vetor A: ";
        cin >> vetA[i];
    }
    //Atribui��o de valores do vetor B
    for(int i=0;i<tamB;i++){
        cout << "Digite o valor da " << i+1 << "� posi��o do vetor B: ";
        cin >> vetB[i];
    }

    int cont=0, menor;

    //Saber qual o menor
    if(tamB<tamA){
        menor = tamB;
    }else{
        menor = tamA;
    }
    //Percorre e atribui os valores do vetor at� quem A ou B n�o tenham mais valores a atribuir ao vetor v.
    for(int i=0;i<menor*2;i++){
        if(i%2==0){
            vetInt[i] = *(vetA++);
        }else{
            vetInt[i] = *(vetB++);
        }
    }
    /Atribui o restante
    if(tamA!=tamB){

        if(menor==tamA){
            //Caso o vetA seja o menor
            for(int i=0;i<tamB-tamA;i++){
                vetInt[tamA*2] = *(vetB++);
            }
        }else{
            //Caso o vetA seja o menor
            for(int i=0;i<tamA-tamB;i++){
                vetInt[tamB*2] = *(vetA++);
            }
        }
    }

    //Restaura ponteiros as posi��es iniciais
    vetB-=tamB;
    vetA-=tamA;

    //Atibui os valores do vetor A ao vetor de uni�o
    for(int i=0;i<tamA;i++){
        vetUni[i] = *(vetA++);
    }
    //Atibui os valores do vetor B ao vetor de uni�o
    for(int i=tamA;i<tamB+tamA;i++){
        vetUni[i] = *(vetB++);
    }

    //Impress�o na tela
    cout << "Vetor unido" << endl;
    for(int i=0;i<tamUni;i++){
        cout << vetUni[i] << " ";
    }
     cout << "\n\n\n";

    cout << "Vetor Intercalado"<< endl;
    for(int i=0;i<tamInt;i++){
        cout << *(vetInt++) << " ";
    }
    cout << "\n";
}

int main(){
    //Corre��o de palavras acentuadas
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    int* vetA = NULL;
    int* vetB = NULL;
    int* vetUni = NULL;
    int* vetInt = NULL;
    int tamA,tamB,tamUni, tamInt;

    //Atribui��o de valores que decidiram os tamanhos de A e vc
    cout << "Digite o tamanho do vetor  A : ";
    cin >> tamA;

    cout << "Digite o tamanho do vetor  B :  ";
    cin >> tamB;

    //Incia fun��o
    uniaoIntersecao(vetA,tamA,vetB,tamB,vetUni,tamUni,vetInt,tamInt);

return 0;
}

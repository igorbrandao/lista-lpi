/*! \equacao.cpp
 * Brief description: Main program.
 *
 * Detailed description: Check if a point is inside or outside
 * from a rectangle.
*/

#include <iostream>
#include <cstdlib>
#include <clocale>

using namespace std;

int main()
{
	setlocale(LC_ALL,"Portuguese");

	// Auxiliary variables
	int vet_X[3];
	int vet_y[3];
	int aux = 0, cont = 0;

	// Data input
	do
	{
		for( int i = 0; i < 3; i++)
		{
			cout << "Type the x coordinate from point " << i+1 << ": " << endl;
			cin >> vet_X[i];

			cout << "Type the y coordinate from point " << i+1 << ": " << endl;
			cin >> vet_y[i];
		}

		// Check if coordinates refer to a rectangle
		if( vet_X[0] != vet_X[1] && vet_y[0] != vet_y[1] )
		{
			cont = 1;
		}
		else
		{
			cout << "The points informed can't build a rectangle" << endl;
			cout << "Please, try again." << endl;
		}
	} while(cont==0);

	// Make sure the first point will be on the left
	if( vet_X[1]<vet_X[0] )
	{
		aux = vet_X[1];
		vet_X[1] = vet_X[0];
		vet_X[0] = aux;

		aux = vet_y[1];
		vet_y[1] = vet_y[0];
		vet_y[0] = aux;
	}

	//Verifica��o das posi��es possiveis do tri�ngulo
    if(vet_X[2]<vet_X[0] || vet_X[2]>vet_X[1])
    {
        cout << "O ponto esta fora do tri�ngulo" << endl;
    } 
    else
    {
        if(vet_y[2]<vet_y[0] || vet_y[2]>vet_y[1])
        {
            cout << "O ponto esta fora do tri�ngulo" << endl;
        }
        else
        {
            if(vet_X[2]==vet_X[0] || vet_X[2]==vet_X[1] || vet_y[2]==vet_y[0] || vet_y[2]==vet_y[1])
            {
                cout << "O ponto t� na borda do tri�ngulo" << endl;
            } 
            else
            {
                cout << "O ponto t� dentro do tri�ngulo" << endl;
            }
        }
    }

    system("pause");
	return EXIT_SUCCESS;
}

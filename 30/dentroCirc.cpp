#include <iostream>
#include <locale.h>
#include <math.h>

using namespace std;

typedef struct _Ponto {
    int x;
    int y;
} Ponto;

//Ver se ta dentro do circulo
int dentroCirc(const Ponto* c, const Ponto* p, const int r){
    //Calcular distancia do ponto central
    float tamanho = (float)sqrt((p->x-c->x)*(p->x-c->x)+(p->y-c->y)*(p->y-c->y));

    if(tamanho<r){
        return 1;
    }else{
        return 0;
    }
}

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL, "Portuguese");
    //Vari�veis
    Ponto c,p;
    int r;

    //Atribui��es de valores
    cout << "Digite o valor do raio: ";
    cin >> r;

    cout << "Digite o valor de x do centro do circulo: ";
    cin >> c.x;

    cout << "Digite o valor de y do centro do circulo: ";
    cin >> c.y;

    cout << "Digite o valor de x do ponto p: ";
    cin >> p.x;

    cout << "Digite o valor de y do ponto p: ";
    cin >> p.y;

    //Fun��o
    if(dentroCirc(&c,&p,r)){
        cout << "Est� dentro do c�rculo";
    }else{
        cout << "N�o est� dentro do c�rculo";
    }

return 0;
}

#include <iostream>
#include <locale.h>

using namespace std;

int mdc(int m,int n){
    if(m>n){
        return mdc(n,m);
    }else{
        if(m<n){
            return mdc(n-m, m);
        }else{
            return m;
        }
    }
}

int mdcEuclides(int m, int n){
    if(n==0){
        return m;
    }else{
        return mdcEuclides(n,m%n);
    }
}

int coPrimos(int m, int n){
    if(mdcEuclides(m,n)==1){
        return 1;
    }else{
        return 0;
    }
}

int main(){
    //Corrige palavras acentuadas
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    int n,m;

    cout << "Digite o valor de m: ";
    cin >> m;

    //Atribui��o de valores
    cout << "Digite o valor de n: ";
    cin >> n;

    cout << "MDC :" << mdcEuclides(m,n) << endl;
    cout << "MDC no Euclides: " << mdc(m,n) << endl;
    if(coPrimos(m,n)){
        cout << "S�o co-primos" << endl;
    }else{
        cout << "N�o s�o co-primos" << endl;
    }
return 0;
}

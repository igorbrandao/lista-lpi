#include <iostream>
#include <locale.h>

using namespace std;
//Fun��o calcula potencia    
int expo(int base, int expo){
    int aux=1;
    for(int i=0;i<expo;i++){
        aux*=base;
    }
    return aux;
}
//Calcula o y depenendo do x informado.    
int calcula(int nums[]){
    //Vari�veis
    int aux=0;
    //L�gica principal 
    for(int x=0;x<8;x+=2){
        aux+=nums[x]* expo(nums[8],nums[x+1]);
    }
    return aux;
}
//Imprime na tela a fun��o com todos os seus valores
void avalia(int nums[]){
    //Vari�veis
    int valor;
    int result=calcula(nums);
    //Come�ando impress�o 
    cout << "f(x)=";

    //Numeros acompanhados por 'x'
    for(int i=0;i<nums[1]*2;i+=2){
        cout << nums[i] << "x";
        //Numeros que tenham '^'
        if(nums[i+1]>1){
            cout << "^";
            cout<<nums[i+1];
        }

        //sinal usado para n�mero posterior
        //Se for positivo:
        if(nums[i+2]>0){
            cout << " + ";
        } else{
            //Se for negativo:
            if(nums[i+2]<0){
                cout << " - ";
                //Exclui o sinal;
                nums[i+2]*=-1;
            }
        }
        valor = i;
    }
    cout << nums[valor+2];
    //Fim da impress�o
    cout << ", f(" << nums[8] << ") = " << result; 
}

int main(){
    setlocale(LC_ALL,"Portuguese");
    int nums[9]={4,2,-2,1,5,0,0,0,5};
    int aux=0;

    //Caso queira usar outros valores
    /*
    for(int i=0;i<8;i++){
        cout << "Digite o n�mero:";
        cin >> nums[i];
    }
    */
    //Imprime resultado
    cout << "Resultado: " << calcula(nums) << endl;

    //Imprime na tela
    avalia(nums);
return 0;
}

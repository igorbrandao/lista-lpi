/*! \equacao.cpp
 * Brief description: Main program.
 *
 * Detailed description: Calculate the second degree equation.
*/

#include <iostream>
#include <cmath>

using namespace std;

int raizes ( float a, float b, float c, float * x1, float * x2 )
{
	// Auxiliary variable;
	float delta;
	delta = pow(b,2) - 4*a*c;

	// Check if equation has at least one root
	if ( delta < 0 ) 
	{
		x1 =0;
		x2 = 0;
		return 0;
	} 
	else
	{
		*x1 = ((b) + sqrt(delta))/(2*a);
		*x2 = ((-b) + sqrt(delta))/(2*a);	
		if( delta == 0 )
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
}

int main( int argc, char const *argv[] )
{
	// Auxiliary varibles
	float a,b,c;
	float x1, x2;

	// Data input
	cout << "Digite o valor de \"a\": ";
	cin >> a;
	cout << "Digite o valor de \"b\": ";
	cin >> b;
	cout << "Digite o valor de \"c\": ";
	cin >> c;

	// Print outputs
	cout << raizes(a,b,c,&x1,&x2) << endl;
	cout << "Raiz x1: "<< x1 << endl;
	cout << "Raiz x2: "<< x2 << endl;

	return 0;
}
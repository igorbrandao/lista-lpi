/*! \soma_pares.cpp
 * Brief description: Drive to even sum.
 *
 * Detailed description: Receive a couple of int numbers indeterminated times
 * and print the sequence from the first one for the second one times.
*/
#include <iostream>

using namespace std;

int main()
{
  // Auxiliary variables
  auto m(0);
  auto n(0);

  // Input
  cout << "Entre com um par de inteiros [m, n] (Ctrl+d p/ encerrar): " << endl;

  while ( cin >> m >> n )
  {
      // Print result
      cout << "[ ";
      for ( auto i(0); i < n; ++i )
      {
          cout << (m + i) << " ";
      }
      cout << "] " << endl << endl;
      cout << "Entre com um par de inteiros [m, n] (Ctrl+d p/ encerrar): " << endl;
  }

  // Program return
  return EXIT_SUCCESS;
}
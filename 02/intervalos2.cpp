/*! \negativo5.cpp
 * Brief description: Drive to test classification.
 *
 * Detailed description: Receive indeterminated number of values
 * and classificate in different classes.
*/
#include <iostream>

using namespace std;

int main()
{
  // Auxiliary variables
  auto class01(0); // Numbers from 0 to 25
  auto class02(0); // Numbers from 25 to 50
  auto class03(0); // Numbers from 50 to 75
  auto class04(0); // Numbers from 75 to 100
  auto class05(0); // Numbers greater than 100

  // Array input
  int v[] = {1, 2, 5, 10, 20, 25, 50, 65, 75, 89, 95, 99, 100, 102};
  int sz = sizeof(v)/sizeof(int); // Determine the array length

  // Input
  cout << endl << "Entre com valores inteiros (Ctrl+d p/ encerrar): " << endl << endl;

  for ( auto i(0); i < sz; ++i )
  {
      // Check the class
      if ( v[i] >= 0 && v[i] < 25 )
        class01 += 1;
      else if ( v[i] >= 25 && v[i] < 50 )
        class02 += 1;
      else if ( v[i] >= 50 && v[i] < 75 )
        class03 += 1;
      else if ( v[i] >= 75 && v[i] <= 100 )
        class04 += 1;
      else
        class05 += 1;
  }

  cout << "Numeros de 0 a 25:\t" << class01 << " ocorrencias\t" << ((float)class01/sz) * 100 << '%' << endl;
  cout << "Numeros de 25 a 50:\t" << class02 << " ocorrencias\t" << ((float)class02/sz) * 100 << '%' << endl;
  cout << "Numeros de 50 a 75:\t" << class03 << " ocorrencias\t" << ((float)class03/sz) * 100 << '%' << endl;
  cout << "Numeros de 75 a 100:\t" << class04 << " ocorrencias\t" << ((float)class04/sz) * 100 << '%' << endl;
  cout << "Numeros > 100:\t\t" << class05 << " ocorrencias\t" << ((float)class05/sz) * 100 << '%' << endl << endl;

  // Program return
  return EXIT_SUCCESS;
}
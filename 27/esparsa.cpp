#include <iostream>
#include <locale.h>

using namespace std;

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL,"Portuguese");

    //Vari�veis
    int m[10][10]={0};
    int cont=1, contnums=0;

    //Preenche com valores n�o nulos
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            m[i][j] = cont%10;
            cont++;
        }
    }

    //Imprime na tela
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            cout << m[i][j] << " ";
        }
        cout << "\n";
    }

    //Contar quantos termos s�o n�o nulos
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            if(m[i][j]){
                contnums++;
            }
        }
    }

    //Matriz condensada
    int **mc;

    //Alocando mem�ria

    //Aloca os ponteiros
    mc = new int*[contnums];

    //Aloca o tamanho de cada ponteiro
    for(int i=0;i<contnums;i++){
        mc[i] = new int[3];
    }

    //Vari�veis
    int contador=0;

    //Atribuindo valores na matriz condensada
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            if(m[i][j]>0){
                mc[contador][0] = m[i][j];
                mc[contador][1] = i;
                mc[contador][2] = j;
                contador++;
            }
        }
    }


    //Impress�o de valores
    cout << "N�meros\t\tLinha\t\tColuna\t\t" << endl;
    for(int i=0;i<contnums;i++){
            cout << mc[i][0] << "\t\t"<< mc[i][1] << "\t\t"<< mc[i][2] << endl;
    }


return 0;
}

#include <iostream>
#include <locale.h>

using namespace std;

//Formula recussiva para multiplica��o
int multSoma(int m, int n){
  if(n==0){
    return 0;
  }else{
    return multSoma(m,n-1) + m;
  }
}

//Formula recussiva para exponencia��o
int expoMult(int m, int n){
  if(n==0){
    return 1;
  }else{
    return expoMult(m,n-1) * m;
  }
}
//Formula recussiva para exponencia��o por somas
int expoSoma(int m, int n){
    if(n==1){
        return m;
    }else{
        return expoSoma(m,n-1) + m*(n-1)*n ;
    }
}

int sucessor(int* i, int* j){
    i+=1;
    j+=1;
}

int main(){
    //Corrige palavras acentuadas
    setlocale(LC_ALL,"Portuguese");
    //Vari�veis
    int n,m;

    cout << "Digite o valor de m: ";
    cin >> m;

    //Atribui��o de valores
    cout << "Digite o valor de n: ";
    cin >> n;

    //Multiplica�ao de valores
    cout << "Resultado da multiplica��o �: " << multSoma(m,n) << endl;

    cout << "Resultado da exponencia��o �: " << expoMult(m,n) << endl;

    cout << "Resultado da exponencia��o �: " << expoSoma(m,n) << endl;

    sucessor(&n, &m);

    cout << "Sucessor dos numeros s�o : " << m << n << endl;

return 0;
}

#include <iostream>
#include <locale.h>

typedef struct _Vetor {
    float x;
    float y;
    float z;
} Vetor;

using namespace std;

//Soma dos vetores e armazena em v3, por meio de um vetor ponteiro res.
void soma(const Vetor *v1, const Vetor *v2, Vetor **res ){
    (*res)->x = v1->x + v2->x;
    (*res)->y = v1->y + v2->y;
    (*res)->z = v1->z + v2->z;
}

//Outra fun��o que soma
Vetor soma2(const Vetor *v1, const Vetor *v2 ){
    Vetor v3;

    v3.x = v1->x + v2->x;
    v3.y = v1->y + v2->y;
    v3.z = v1->z + v2->z;

    return v3;
}

int main(){
    //Permite palavras com acentos
    setlocale(LC_ALL,"Portuguese");
    //Vari�veis
    Vetor v1,v2,v3;
    Vetor* res = &v3;

    //Atribui��o de valores
    cout << "Digite o valor de x do vetor 1: ";
    cin >> v1.x;

    cout << "Digite o valor de y do vetor 1: ";
    cin >> v1.y;

    cout << "Digite o valor de z do vetor 1: ";
    cin >> v1.z;

    cout << "Digite o valor de x do vetor 2: ";
    cin >> v2.x;

    cout << "Digite o valor de y do vetor 2: ";
    cin >> v2.y;

    cout << "Digite o valor de z do vetor 2: ";
    cin >> v2.z;
    cout << "\n\n";

    //Descomente para usar a fun��o de soma
    //soma(&v1,&v2,&res);

    //Descomente para usar a fun��o soma2
    //v3 = soma2(&v1,&v2);

    //Impress�o dos valores
    cout << "Valor de x do vetor resultante: " << v3.x << endl;
    cout << "Valor de y do vetor resultante: " << v3.y << endl;
    cout << "Valor de z do vetor resultante: " << v3.z << endl;

return 0;
}

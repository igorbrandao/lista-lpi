/*! \vet5.cpp
 * Brief description: Main program.
 *
 * Detailed description: Compare an int value with an entire array.
*/

#include <iostream>

using namespace std;

void print_array( int A[ ], int sz_, string msg_ )
{
    cout << msg_ << ": [ ";
    for( auto i(0); i < sz_ ; ++i )
    {
        // Print array
        cout << A[i] << " ";
    }
    cout << "] " << endl << endl;
}

void empty_array( int vet_[ ], int sz_ )
{
    for( auto i(0); i < sz_ ; ++i )
    {
        // Empty array
        vet_[i] = 0;
    }
}

void fill_arrays( int vet_[], int vetorA[], int vetorB[], int sz_, int sz2_ )
{
	// Auxiliary variables
	auto cont_a(0);
	auto cont_b(0);

	for ( auto i(0); i < sz2_; ++i )
	{
		// Check even or odd
		if ( (vet_[i] % 2) == 0 ) // even
		{
			vetorA[cont_a] = vet_[i];

			// Check if its full
			if ( cont_a == (sz_ - 1) )
			{
				print_array(vetorA, sz_, "vector A");
				empty_array(vetorA, sz_);
				cont_a = 0;
			}
			else				
				cont_a++;
		}
		else // odd
		{
			vetorB[cont_b] = vet_[i];

			// Check if its full
			if ( cont_b == (sz_ - 1) )
			{
				print_array(vetorB, sz_, "vector B");
				empty_array(vetorB, sz_);
				cont_b = 0;
			}
			else
				cont_b++;
		}
	}
}

int main()
{
	// Auxiliary variables
	int len = 5; /* Change to set-up the sub vector length */
	int vetorA[len];
 	int vetorB[len];
 	int vet[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
 				 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
 				 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
    int sz = sizeof(vet)/sizeof(int); // Determine the array length

    // Print original vector
    print_array(vet, sz, "vet");

 	// Call function to fill arrays
 	fill_arrays(vet, vetorA, vetorB, len, sz);

 	return 0;
}
#include <iostream>
#include <cstdlib>

using namespace std;

//Imprime a sequencia de fibonacci at� o n-�ssimo termo]
void nessimoFib(int* &v, int n, int& tam)
{
    int fib=1, ant1=1, ant2=0, cont=0;
    //L�gica principal,ira servir aqui para saber quantos termos s�o
    for(int i=0;i<n;i++)
    {
        cont++;
        fib = ant1 + ant2;
        ant2 = ant1;
        ant1 = fib;
    }

    //Tamanho que ser� alocado dinamicamente no vetor.
    tam = cont;

    v = new int[tam];

    fib=1, ant1=1, ant2=0, cont=0;
    //L�gica principal, servir� para salvar os valores no vetor
    for(int i=0;i<n;i++)
    {
        v[cont]=fib;
        fib = ant1 + ant2;
        ant2 = ant1;
        ant1 = fib;
        cont++;
    }
}
//Faz a sequencia at� o valor especificado
void fibMenor(int* &v, int n, int& tam)
{
    //Vari�veis
    int fib=1, ant1=1, ant2=0, cont=0;
    
    //L�gica principal,ira servir aqui para saber quantos termos s�o
    while(fib <= n)
    {
        cont++;
        fib = ant1 + ant2;
        ant2 = ant1;
        ant1 = fib;
    }
    
    //tamanho para q se posso alocar dinamicamente o vetor
    tam = cont;

    v = new int[tam];


    fib=1, ant1=1, ant2=0, cont=0;
    
    //L�gica principal, servir� para salvar os valores no vetor
    while(fib <= n)
    {
        v[cont]=fib;
        fib = ant1 + ant2;
        ant2 = ant1;
        ant1 = fib;
        cont++;
    }
}


//Imprimi a pir�mide de fibonacci na tela
void piramideFib(int* &v, int n)
{
    //Vari�veis
    int tam=0, contesp=0;
    int* v2 = NULL;
    //Calcula n-�ssimo termo da sequ�ncia
    nessimoFib(v,n,tam);

    //espa�amento a direita 
    int varia = (tam*5);

    // Parte de cima da pir�mide
    for(int i=0;i<tam;i++)
    {
        //Imprime espa�amento
        for(int j=0;varia>j;j++)
        {
            cout << " ";
        }

        v+=i;
        //Espa�amento diferenciado se for com 1 digito ou 2
        if((*v)>7)
        {
            varia-=3;
            contesp=1;
        }
        else
        {
            varia-=2;
        }
        
        //Imprime n�meros da sequencia correspondes a determinada linha
        for(int x=i;x>=0;x--)
        {
            cout << *(v--) << " ";
        }
        v++;

        cout << "\n";
    }

    //Corrigindo espa�amentos
    if(contesp){
        varia+=6;
    }else{
        varia+=3;
    }

    //Voltando o ponteiro ao primeiro elemnto
    v+=tam-2;

    //Impress�o da parte de baixo da pir�mide
    for(int i=0;i<tam-1;i++)
    {
        //Imprime espa�amento
        for(int j=0;varia>j;j++)
        {
            cout << " ";
        }

        //Espa�amento diferenciado se for com 1 digito ou 2
        if((*v)>10)
        {
            varia+=3;
        }
        else
        {
            varia+=2;
        }

        //Imprime n�meros da sequencia correspondes a determinada linha 
        for(int x=tam-i-2;x>=0;x--)
        {
            cout << *(v--) << " ";
        }
        v+=tam-i-2;

        cout << "\n";
    }
}

int main()
{
    int num, tam;
    int* v= NULL;

    cout << "Digite o valor do n�mero a ser usado nas fun��es: ";
    cin >> num;

    fibMenor(v,num,tam);
    v += tam-1;
    for(int i =0;i<tam;i++){
        cout << *(v++) << " ";
    }
    
    
    nessimoFib(v,num, tam);

    for(int i=0;i<tam;i++){
        cout << *(v++) << " ";
    }
    

    piramideFib(v,num);

    system("pause");
    return 0;
}

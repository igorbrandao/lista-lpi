/*! \troca_seguintes.cpp
 * Brief description: Change the odd elements with the next even.
 *
*/
#include <iostream>

using namespace std;

int check_even( int value )
{
	return (value % 2);
}

void print_array( int A[ ], int sz )
{
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Print array
		cout << A[i] << " ";
	}
	cout << "] " << endl << endl;
}

void change_elements( int A[ ], int sz )
{
	int aux1, aux2;

	for( int i(0) ; i < (sz - 1); ++i )
	{
		// Check if odd
		if ( check_even(A[i]) == 1 )
		{
			aux1 = A[i];
			aux2 = A[(i+1)];
			A[i] = aux2;
			A[(i+1)] = aux1;
			i++;
		}
	}
}

int main()
{
	// Auxiliary variables
	int vet[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
				   11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	int sz = sizeof(vet)/sizeof(int); // Determine the array length

	#ifdef MANUAL_INPUT
		// Array input
		auto fxSz(20);
		for( auto i(0); i < fxSz ; ++i )
		{
			cout << "Digite o elemento " << (i+1) << " do vetor: " << endl;
			cin >> vet[i];
		}
	#endif

	// Print original array
	print_array(vet, sz);

	// Change elements
	change_elements(vet, sz);

	// Print new array
	print_array(vet, sz);

	return 0;
}
/*! \main.cpp
 * Brief description: Main program.
 *
 * Detailed description: Compare an int value with an entire array.
*/

#include <iostream>
#include "q6.h"

using namespace std;

int main()
{
  // Array input
  int v[] = {1, 2, 5, 10, 20, 25, 50, 65, 75, 89, 95, 99, 100, 102};
  int sz = sizeof(v)/sizeof(int); // Determine the array length
  int x = 51;

  cout << endl << ">>> v: [ ";
  for ( auto &e : v )
    cout << e << " ";
  cout << "]" << endl;

  cout << endl << "[Qtd de numeros maiores que " << x << ": " << maiores(v, sz, x) << "]" << endl << endl;

  // Program return
  return EXIT_SUCCESS;
}
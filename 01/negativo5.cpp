/*! \negativo5.cpp
 * Brief description: Drive to test negative numbers.
 *
 * Detailed description: Receive 5 values and test which one
 * is negative.
*/
#include <iostream>

using namespace std;

int main()
{
  // Auxiliary variables
  auto x(0), negative(0);

  // Input
  for ( auto i(0); i < 5; i++ )
  {
      cin >> x;

      if ( x < 0 )
        negative += 1;
  }

  cout << "Vc digitou " << negative << " numeros negativos!" << endl;

  // Program return
  return EXIT_SUCCESS;
}
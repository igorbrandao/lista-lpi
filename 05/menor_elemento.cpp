/*! \menor_elemento.cpp
 * Brief description: Find the smallest element in an array.
 *
 * Detailed description: Create a real number array and find the smallest
 * element, giving his value and position.
*/
#include <iostream>

using namespace std;

int main()
{
	// Auxiliary variables
	auto posic(0);
	float vet[] = {2.f, 1.5, 0.9, 10.5, 6, 152, 9.8, 8.2, 3.5, 0.3,
				   0.4, 0.35, 100.8, 25.3, 0.12, 1.23, 4, 7, 8, 22};
	int sz = sizeof(vet)/sizeof(int); // Determine the array length

	#ifdef MANUAL_INPUT
		// Array input
		auto fxSz(20);
		for( auto i(0); i < fxSz ; ++i )
		{
			cout << "Digite o elemento " << (i+1) << " do vetor: " << endl;
			cin >> vet[i];
		}
	#endif

	// Check the smallest
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Update the smallest position
		if ( vet[posic] > vet[i] )
			posic = i;

		// Print array
		cout << vet[i] << " ";
	}
	cout << "] " << endl << endl;

	// Show the smallest element
	cout<<"Element: \t" << vet[posic] << endl;
	cout<<"Position: \t" << posic << endl << endl;

	return 0;
}